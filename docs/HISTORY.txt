Changelog
=========

1.0rc2 (unreleased)
-------------------

- Nothing changed yet.


1.0rc1 (2014-01-09)
-------------------

- Fixed html validation error in the footer [ronniestevens]

1.0a15 (2013-12-18)
-------------------

- Fixed mixed search with hashtags and username for working with API 1.1 [cekk]


1.0a14 (2013-09-25)
-------------------

- Nothing changed yet.


1.0a13 (2013-09-09)
-------------------

- Fixed reg-exs when there is an underscrore in the name [ralphjacobs].


1.0a12 (2013-08-19)
-------------------

- Fixed userPicture width and heigth so they don't blow up the portlet [ralphjacobs].


1.0a11 (2013-07-08)
-------------------

- Move consumer key/secret and application token key/scret to control panel. [stevenlooman]
- Fix caching mechanism in twitter.py. [stevenlooman]


1.0a10 (2013-06-21)
-------------------

- Use PID instead of username for cache directory. [stevenlooman]
- Add upgrade step that simply applies all our import steps,
  since almost everything has changed.
  [maurits]
- Made twitter.py compatible with python24. [stevenlooman]


1.0a9 (2013-06-19)
------------------

- keep portlet working on plone <4.1
  [maartenkling]

1.0a8 (2013-06-18)
------------------

- Removed client-side mechanism, as Twitter no longer support anonymous requests. [stevenlooman]
- Fix traceback when an existing fourdigits.portlet.twitter was upgraded. [stevenlooman]


1.0a7 (2013-06-18)
------------------

- Nothing changed yet.


1.0a6 (2013-06-18)
------------------

- Twitter client library (twitter.py) updated to 1.0, to use v1.1 of the Twitter API [ralphjacobs]
- Added proxy and timeout information to Twitter API [fabiosurrage]
- Added Control Panel to the Product (Thanks to Manabu TERADA - https://bitbucket.org/terapyon)
- Added cache for UserInfo and Tweets (Thanks to Manabu TERADA - https://bitbucket.org/terapyon)
- Updated translation to PT-BR [fabiosurrage]
- Updated Twitter portlet to use Twitter client 1.0 [stevenlooman]

1.0a5 (unreleased)
------------------

- Fixed search for multiple terms. Now combine search terms [cekk]
- Added "from_user" argument in twitter library to allow combined search between userid and text [cekk]
- Added field in portlet config to allow combined search [cekk]
- Fixed translation message ids [cekk]
- Added italian translation [cekk]

1.0a4 (2012-09-27)
------------------

- Return only first part of language in case of nl-nl, we only register nl.js files [kingel]


1.0a3 (2012-09-05)
------------------

- Replace jquery.easydate.js for collective.js.moment, remove not needed
  javascripts from portal_javascript. [kingel]


1.0a2 (2012-08-10)
------------------
- Make sure old portlet assignments keep working [ralphjacobs]


1.0a (2012-08-10)
-----------------
- Added support for clientside rendering using the libs from http://tweet.seaofclouds.com/
  You can turn this on in the portlet settings.
  (note: relative dates are in dutch only at the moment) [kingel]
- Fixed serverside caching by using a os.getpid() for generating tmp dir to avoid permission problems [ralphjacobs]

0.9.2
-----
- Adding original permission for Site Administrator [terapyon]
- Adding test code for portlet base [terapyon]

0.9.1
-----
- Fixed missing files in 0.9 package [sjoerdve]

0.9
---
- Fix invalid CSS [sjoerdve]

0.8
---
- Include alt text for twitter images [ralphjacobs]

0.7
---
- Made portlet backwards compatible with Plone 3 versions again [ralphjacobs]

0.6
---
- Added support for Plone 4.1 [sjoerdve]
- Fixed double name bug when tweets are searched [sjoerdve]
- Minor code cleanup [ralphjacobs]

0.5
---
- Added support for displaying twitter user profile info [ralphjacobs]
- Fixed caching bug, fixes ticket #4 [ralphjacobs]
- Based on new upstream python-twitter library 0.8.2 [ralphjacobs]
- Made implemetation compatible with new upstream library [ralphjacobs]
- Added multilanguage support [ralphjacobs] [rafahela]
- Added dutch translations [ralphjacobs]
- Added Portugese (Brazilian) translations [rafahela]

0.4
---
- Added classes to fields in template. [robgietema]
- Removed markup from python files [robgietema]
- Extended matching regext for auto-link URLs [ralphjacobs]

0.3
---
- Added support for retweets based on username [ralphjacobs]

0.2
---
- Fix release, refactored tweet results logic [martijn4d]

0.1
---
- Initial version, based on collective.twitter
  and python-twitter 0.7-devel for searching
- (Multi) Language support for twitter feeds
- Better text filter for curse words
- Multi search support
- User picture support
